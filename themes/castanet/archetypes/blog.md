+++
Description = ""
Date = {{ .Date }}
PublishDate = {{ .Date }} # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00" #can use this in bash too date +%Y-%m-%dT%H:%M:%S%z
title = ""
draft = true
#blog_banner = ""
blog_image = "img/host/calaveraJP.jpeg"
images = ["img/episode/default-social.jpg"]
Author = "rastacalavera"
#aliases = ["/##"]
#series = [""]
#tags = [""]
+++
