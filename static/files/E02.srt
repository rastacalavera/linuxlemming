1
00:00:00,000 --> 00:00:19,720
Hello, and welcome to episode
2 of the Linux Lemming.

2
00:00:19,720 --> 00:00:27,600
So for this one, I decided to give it a title,
dealing with a little bit of swagger, because

3
00:00:27,600 --> 00:00:36,280
the end game of it was to follow the Linux
server IO documentation on setting up a Docker

4
00:00:36,280 --> 00:00:44,160
environment and then kind of protecting everything
behind a Nginx reverse proxy and let's encrypt

5
00:00:44,160 --> 00:00:50,200
SSL certification using
their swag container.

6
00:00:50,200 --> 00:00:59,760
So to get started with this, it was a very,
very frustrating experience, not to Linux

7
00:00:59,760 --> 00:01:07,840
server IO, but getting a Raspberry Pi
running the latest Ubuntu server.

8
00:01:07,840 --> 00:01:09,760
And this is not an
old Raspberry Pi.

9
00:01:09,760 --> 00:01:15,320
This is the Raspberry Pi 4, a really common
thing that a lot of people are buying.

10
00:01:15,320 --> 00:01:19,640
And everything that I've seen online, people
are saying you can run it off of SSDs and

11
00:01:19,640 --> 00:01:21,560
external hard drives
and things like that.

12
00:01:21,560 --> 00:01:27,480
So you're not constantly slamming an SD card
with read and write cycles, which long term

13
00:01:27,480 --> 00:01:30,800
can make that card
not last very long.

14
00:01:30,800 --> 00:01:39,240
So I previously had a Raspberry Pi 4 server
set up and I had some mismanagement with it

15
00:01:39,240 --> 00:01:44,000
and it was just easier to blow the whole thing
away and let it sit on a desk for a while.

16
00:01:44,000 --> 00:01:48,220
And I recently decided, okay,
time to spin it back up.

17
00:01:48,220 --> 00:01:58,080
So with this whole SSD thing, an external SD
thing, a couple years ago, well, I don't

18
00:01:58,080 --> 00:02:02,520
even know if it was that long ago, but anyway,
the ability to do this, to run the OS off

19
00:02:02,520 --> 00:02:06,800
of an external drive has been around
for a bit, but it was kind of hacky.

20
00:02:06,800 --> 00:02:10,400
And I had been hearing that now it's
baked into the firmware and everything.

21
00:02:10,400 --> 00:02:16,880
And really, all you need to do is get
the Raspberry Pi OS latest version.

22
00:02:16,880 --> 00:02:20,400
Make sure your firmware
for the Pi is updated.

23
00:02:20,400 --> 00:02:26,240
And then after that, you can flash an image to
any external drive, plug it in and be good

24
00:02:26,240 --> 00:02:27,240
to go.

25
00:02:27,240 --> 00:02:35,560
I've heard of hybrid solutions where people
keep the boot image on an SD card, but then

26
00:02:35,560 --> 00:02:38,560
do all the reading and writing
from an external drive.

27
00:02:38,560 --> 00:02:40,000
And I don't want to
mess around with that.

28
00:02:40,000 --> 00:02:42,680
I just want everything
on an external drive.

29
00:02:42,680 --> 00:02:47,240
So what I did, I made sure
that my firmware was updated.

30
00:02:47,240 --> 00:02:54,600
I flashed Ubuntu Server 2004 using Etcher
and plugged it in and didn't work.

31
00:02:54,600 --> 00:03:00,960
I did have my Pi plugged into an external
monitor so I could keep an eye on the process.

32
00:03:00,960 --> 00:03:07,760
And I kept getting all these weird errors
that my ethernet wasn't being detected and

33
00:03:07,760 --> 00:03:11,360
it just wouldn't really
move beyond that.

34
00:03:11,360 --> 00:03:17,240
And of course, I was using ethernet and I
wasn't using wireless or anything like that.

35
00:03:17,240 --> 00:03:23,040
So I thought maybe it was a bad
flash, did it again, same error.

36
00:03:23,040 --> 00:03:26,640
So then I thought, okay, maybe
this is a problem with Etcher.

37
00:03:26,640 --> 00:03:31,920
So I switched over to the Raspberry Pi Imager
app, which is available as a snap, and repeated

38
00:03:31,920 --> 00:03:36,080
the process.

39
00:03:36,080 --> 00:03:37,080
Similar errors.

40
00:03:37,080 --> 00:03:42,000
So then I decided, okay, let's let Raspberry
Pi Imager handle the whole thing.

41
00:03:42,000 --> 00:03:46,480
It can get the image for me, it won't be the
one I downloaded, and it will flash it all

42
00:03:46,480 --> 00:03:48,080
on its own.

43
00:03:48,080 --> 00:03:53,360
When I did it this time, ethernet worked, but
it still didn't recognize a boot environment

44
00:03:53,360 --> 00:03:54,520
variable.

45
00:03:54,520 --> 00:03:58,480
So I was just really
upset at this point.

46
00:03:58,480 --> 00:04:04,640
And this is probably close to 45
minutes in or so of tinkering around.

47
00:04:04,640 --> 00:04:10,200
So now that I got this boot environment variable
error, I go to Google, search for it, came

48
00:04:10,200 --> 00:04:20,240
across a forum post, and long story short, this
is a known issue quote unquote with 2004.

49
00:04:20,240 --> 00:04:24,640
And there's not really a straight answer as
to whether or not this is considered a bug

50
00:04:24,640 --> 00:04:27,880
or expected behavior
and whatever else.

51
00:04:27,880 --> 00:04:34,720
And the only solution that I found was to use
2010, which I didn't want to do, because

52
00:04:34,720 --> 00:04:39,520
if I'm going to be running it as a server,
I want it on the long term support.

53
00:04:39,520 --> 00:04:44,760
But I didn't want to continue to search the
internet and look for solutions, I just wanted

54
00:04:44,760 --> 00:04:45,760
to be up and running.

55
00:04:45,760 --> 00:04:54,680
So bit the bullet, got 2010 64 bit
image, and was going to run with that.

56
00:04:54,680 --> 00:05:01,360
So I do all that through Raspberry
Pi imager, and still didn't work.

57
00:05:01,360 --> 00:05:08,560
So then I thought, okay, maybe maybe the
whole drive just needs to be reformatted.

58
00:05:08,560 --> 00:05:13,200
I've done a bunch of flashing, let's just
wipe the whole darn thing and start over.

59
00:05:13,200 --> 00:05:14,320
Did that.

60
00:05:14,320 --> 00:05:17,640
And this time, everything
booted up just fine.

61
00:05:17,640 --> 00:05:20,260
But I had the desktop image.

62
00:05:20,260 --> 00:05:23,680
So in my haste, I had
clicked the wrong thing.

63
00:05:23,680 --> 00:05:27,120
So I was like, all right,
well desktop works.

64
00:05:27,120 --> 00:05:33,360
Let's just go reformat the drive
again, flash the correct image.

65
00:05:33,360 --> 00:05:36,320
Still didn't work.

66
00:05:36,320 --> 00:05:41,200
And I was just livid
at this point.

67
00:05:41,200 --> 00:05:42,200
I tried.

68
00:05:42,200 --> 00:05:47,240
I said, okay, maybe I just need to forget
Ubuntu and just go with Raspberry Pi OS.

69
00:05:47,240 --> 00:05:49,760
And I did that with
their light image.

70
00:05:49,760 --> 00:05:50,760
And that was working.

71
00:05:50,760 --> 00:05:57,320
I was making progress, I was installing things
like Docker, and then boom, out of nowhere,

72
00:05:57,320 --> 00:06:05,200
something happens, something flips over, and
my drive becomes a read-only file system.

73
00:06:05,200 --> 00:06:07,800
Can't do anything.

74
00:06:07,800 --> 00:06:16,280
Power it all the way down, turn it back on, read
only, won't let me SSHN, like it's infuriating.

75
00:06:16,280 --> 00:06:21,600
So I decided to just start over
again, reflash everything.

76
00:06:21,600 --> 00:06:25,880
And when I was looking around on things on
the internet, I did come across something

77
00:06:25,880 --> 00:06:32,200
that had mentioned that you should have
your external plugged into a USB 2 port.

78
00:06:32,200 --> 00:06:36,160
And I didn't pay much attention to that because
I was like, well, I want the faster speed

79
00:06:36,160 --> 00:06:37,800
of USB 3.

80
00:06:37,800 --> 00:06:43,480
I believe it may be incorrect, but I believe
that you get better power through USB 3.

81
00:06:43,480 --> 00:06:47,760
So I figured that would be good for
the longevity of the drive as well.

82
00:06:47,760 --> 00:06:52,760
But after all this stuff, I was like, let's
just plug it into USB 2 and see what the heck

83
00:06:52,760 --> 00:06:54,560
happens.

84
00:06:54,560 --> 00:06:57,640
So I do that, and it's working.

85
00:06:57,640 --> 00:07:02,440
I install a couple packages,
and it's working.

86
00:07:02,440 --> 00:07:09,800
So now I'm thinking, oh man, maybe I
should try Ubuntu again, do it on USB 2.

87
00:07:09,800 --> 00:07:10,800
So that's what I do.

88
00:07:10,800 --> 00:07:17,160
I format it, reflash 2010
USB 2, and it's working.

89
00:07:17,160 --> 00:07:28,000
So if you try to emulate what I'm doing, save
yourself a step and use USB 2 in the 2010

90
00:07:28,000 --> 00:07:29,000
image.

91
00:07:29,000 --> 00:07:32,360
And hopefully it
works out for you.

92
00:07:32,360 --> 00:07:39,520
Maybe my issues were compounded because
this drive that I'm using is fairly old.

93
00:07:39,520 --> 00:07:47,600
I think I've had it since
2012-ish, maybe earlier.

94
00:07:47,600 --> 00:07:52,560
So maybe it just can't handle USB
3, and it can only be USB 2.

95
00:07:52,560 --> 00:07:54,280
I don't know.

96
00:07:54,280 --> 00:08:00,920
But all I know is that in my situation, I
have to use USB 2, which, unfortunate, but

97
00:08:00,920 --> 00:08:02,760
I'll live with it.

98
00:08:02,760 --> 00:08:07,220
So now I got Ubuntu
2010 up and running.

99
00:08:07,220 --> 00:08:11,560
And I have the default user in there as Ubuntu,
and you've got to change the default password

100
00:08:11,560 --> 00:08:12,720
and all that.

101
00:08:12,720 --> 00:08:16,160
So I want to add in my new
user, Rasta Calavera.

102
00:08:16,160 --> 00:08:24,080
So user command, user add, Rasta Calavera,
chmod, agsudo, figure everything's good.

103
00:08:24,080 --> 00:08:30,280
Log out of the default Ubuntu, try to log
back in as Rasta Calavera, and it's asking

104
00:08:30,280 --> 00:08:31,280
me for a password.

105
00:08:31,280 --> 00:08:38,040
And I'm like, huh, well, when I added my user,
it didn't go through that prompt of like,

106
00:08:38,040 --> 00:08:42,520
enter user password, what's the user's full
name, what room do they reside in, blah, blah,

107
00:08:42,520 --> 00:08:45,840
blah, email, all that stuff that,
you know, in the back of my mind.

108
00:08:45,840 --> 00:08:49,880
And I typically don't add a lot
of users when I do servers.

109
00:08:49,880 --> 00:08:54,680
I just kind of tend to change the password
of the default account and just let it be.

110
00:08:54,680 --> 00:08:58,280
But with the whole Linux learning and everything
I'm doing now, I figured, well, I should just

111
00:08:58,280 --> 00:09:02,520
do everything under Rasta
Calavera, just centralize it all.

112
00:09:02,520 --> 00:09:03,520
And so I was confused.

113
00:09:03,520 --> 00:09:06,680
I was like, what the
heck is going on?

114
00:09:06,680 --> 00:09:12,680
So I log back in as the Ubuntu account, set
a password for Rasta Calavera, log out, log

115
00:09:12,680 --> 00:09:16,000
back in, ls, nothing.

116
00:09:16,000 --> 00:09:21,560
I think even when I did ls, it said
command not found, and I was like, what?

117
00:09:21,560 --> 00:09:25,320
So then I start looking around
and it's, it's not using bash.

118
00:09:25,320 --> 00:09:26,920
There's no tab auto completion.

119
00:09:26,920 --> 00:09:29,080
I can't run basic directories.

120
00:09:29,080 --> 00:09:32,560
I don't know what the
heck is going on.

121
00:09:32,560 --> 00:09:39,480
So I go back to the internet, start doing some
searching, figure out how to define what

122
00:09:39,480 --> 00:09:42,840
shell a user should have.

123
00:09:42,840 --> 00:09:46,640
And it was just like a headache
that didn't need to be there.

124
00:09:46,640 --> 00:09:53,040
So I don't know if this is just my ignorance
or if something changed along the line.

125
00:09:53,040 --> 00:09:59,600
But I feel like creating a new user
is not as easy as it used to be.

126
00:09:59,600 --> 00:10:04,760
So now apparently when you create a new user,
you have to, if you want them to have a home

127
00:10:04,760 --> 00:10:10,440
directory and things like that, you have to
define all of that and also set their password

128
00:10:10,440 --> 00:10:16,720
up separately and define their shell environment,
which okay, I'm fine doing now in the future,

129
00:10:16,720 --> 00:10:21,680
but it would have saved me time knowing
that I had to do that before all of this.

130
00:10:21,680 --> 00:10:27,520
So maybe I'm wrong and if somebody out there
knows better than me and there's a simple

131
00:10:27,520 --> 00:10:34,200
command that gets me to that familiar setting
that I mentioned where it's like username,

132
00:10:34,200 --> 00:10:39,560
user password, user full name, user room
number, user email, blah, blah, blah, blah,

133
00:10:39,560 --> 00:10:43,200
all that crap that you
normally leave blank anyway.

134
00:10:43,200 --> 00:10:45,120
I just don't know
what happened to it.

135
00:10:45,120 --> 00:10:49,960
So anyway, got that all set
up, finally ready to go.

136
00:10:49,960 --> 00:10:56,000
And this is where the tires meet the
road for the Linux learning project.

137
00:10:56,000 --> 00:11:02,760
Now that the server is set up, time to
blindly follow some documentation.

138
00:11:02,760 --> 00:11:05,680
So I head over to the
Linux server.io.

139
00:11:05,680 --> 00:11:11,320
And if you're not familiar with that project,
I have used them for a couple years.

140
00:11:11,320 --> 00:11:16,040
It's been a while since I've actually sat
down and read the documentation and tried

141
00:11:16,040 --> 00:11:19,880
to follow it completely because I
built up my skills previously.

142
00:11:19,880 --> 00:11:23,040
So I figured, all right,
this is going to be good.

143
00:11:23,040 --> 00:11:27,960
And I just start right at their very first
page of documentation and start reading and

144
00:11:27,960 --> 00:11:29,880
clicking through.

145
00:11:29,880 --> 00:11:38,160
And as I was looking at it, I thought, you
know, it would be nice if there's a disclaimer

146
00:11:38,160 --> 00:11:42,200
at the very beginning, kind of like there
was a disclaimer in VS code where it's like

147
00:11:42,200 --> 00:11:46,920
if you're going to continue on with this,
make sure you understand these fundamentals.

148
00:11:46,920 --> 00:11:52,080
And there wasn't one that I saw in the Linux
server documentation, something that would

149
00:11:52,080 --> 00:11:58,040
say like the following guides assume that
you already have Docker installed and that

150
00:11:58,040 --> 00:12:01,680
you already have Docker
composed installed.

151
00:12:01,680 --> 00:12:07,360
Because I feel like a lot of people who are
going to be introduced to this are probably

152
00:12:07,360 --> 00:12:12,080
told from other sources in the community,
hey, if you're going to start using Docker,

153
00:12:12,080 --> 00:12:15,160
Linux server IO is a great
place to get your images from.

154
00:12:15,160 --> 00:12:17,800
You should just use that blah,
blah, blah, blah, blah.

155
00:12:17,800 --> 00:12:19,360
So they may just go
straight there.

156
00:12:19,360 --> 00:12:25,000
And if they're completely new to this, there
is some very crucial steps missing from their

157
00:12:25,000 --> 00:12:32,440
documentation, which they're going to have
to hop on either a search engine or a forum

158
00:12:32,440 --> 00:12:36,760
or a live chat or something
to ask some questions.

159
00:12:36,760 --> 00:12:41,920
So I think it would be beneficial to even
just have a short little two sentence thing

160
00:12:41,920 --> 00:12:45,640
in there saying like, hey, you
need Docker and Docker compose.

161
00:12:45,640 --> 00:12:50,560
If you don't know what those are, or you don't
know how to install them, check this out first.

162
00:12:50,560 --> 00:12:56,040
And it just links you directly to the actual
Docker documentation where they can spend

163
00:12:56,040 --> 00:13:02,320
time reading and installing those two
components and then return to Linux server.

164
00:13:02,320 --> 00:13:05,720
But maybe that's not their
target demographic.

165
00:13:05,720 --> 00:13:11,200
Maybe they're assuming that people are coming
to them with that prior knowledge, which Fair

166
00:13:11,200 --> 00:13:16,280
Play, that's, you know, they can assume whatever
they want, but I think it would be beneficial

167
00:13:16,280 --> 00:13:20,840
if they had just a tiny
little disclaimer in there.

168
00:13:20,840 --> 00:13:27,440
So set up Docker and
ready to get going now.

169
00:13:27,440 --> 00:13:35,560
In the documentation of Linux server, they
recommend that you add in some bash aliases

170
00:13:35,560 --> 00:13:37,840
to kind of help
speed things along.

171
00:13:37,840 --> 00:13:42,480
So you're not having to type
out these big long commands.

172
00:13:42,480 --> 00:13:51,400
And one thing I noticed here is they only
list out one alias for tailing the logs of

173
00:13:51,400 --> 00:13:58,640
a Docker container or Docker image, and
then they kind of move on to other stuff.

174
00:13:58,640 --> 00:14:04,200
And then later on in the documentation, they
have like four or five aliases that they say

175
00:14:04,200 --> 00:14:07,320
that are like good
and handy to have.

176
00:14:07,320 --> 00:14:10,560
So there's a little
disconnect to there.

177
00:14:10,560 --> 00:14:16,800
I think it would be advantageous to just kind
of maybe have that in there twice, like in

178
00:14:16,800 --> 00:14:21,480
the very beginning, instead of just having
the one, just put all of them there again.

179
00:14:21,480 --> 00:14:25,920
And then later on in the documentation, say,
hey, if you forgot this step, you maybe still

180
00:14:25,920 --> 00:14:28,720
want to do it now.

181
00:14:28,720 --> 00:14:32,000
Just because, you know, if you're in there
editing the file, why not just put it all

182
00:14:32,000 --> 00:14:33,000
in at once?

183
00:14:33,000 --> 00:14:35,400
You know, why bother
jumping around?

184
00:14:35,400 --> 00:14:40,040
So my little two cents there about how
the documentation could be improved.

185
00:14:40,040 --> 00:14:44,800
But you go through, you run the standard Docker
run, hello world, you don't have the container,

186
00:14:44,800 --> 00:14:50,560
it pulls it down, it runs it, you get the
output of it, and you're good to go.

187
00:14:50,560 --> 00:14:55,480
Right after that part in the documentation,
they give a quick snippet of key vocabulary

188
00:14:55,480 --> 00:14:57,960
that the user should
be familiar with.

189
00:14:57,960 --> 00:14:59,440
And I love that.

190
00:14:59,440 --> 00:15:03,680
And that would be something that, you know,
I would take a screenshot of and just kind

191
00:15:03,680 --> 00:15:05,640
of keep off to the side.

192
00:15:05,640 --> 00:15:09,720
So if I'm reading some documentation later
on, I'm like, wait a second, how does that

193
00:15:09,720 --> 00:15:10,720
word apply?

194
00:15:10,720 --> 00:15:13,120
What am I actually, if I'm going to ask a
question, I want to make sure I'm using the

195
00:15:13,120 --> 00:15:16,960
right words, that would be a good thing
to have to just quick reference.

196
00:15:16,960 --> 00:15:23,240
So kudos for that, love it, users
should keep a copy of it somewhere.

197
00:15:23,240 --> 00:15:28,080
As I was reading through, when they start
talking about volumes and where volumes should

198
00:15:28,080 --> 00:15:38,040
be mounted, they had it as slash home slash
user slash app data slash the container name

199
00:15:38,040 --> 00:15:41,480
and the example they
used Heimdall.

200
00:15:41,480 --> 00:15:45,400
And you know, a new user, that's not going to
make a big difference, but coming in from

201
00:15:45,400 --> 00:15:49,960
the perspective of someone who previously
read their documentation, and has used their

202
00:15:49,960 --> 00:15:54,800
containers in the past, I was really confused
reading that because I was like, wait a minute,

203
00:15:54,800 --> 00:16:00,320
I thought they had a whole spiel about how
to keep your persistent data, it should be

204
00:16:00,320 --> 00:16:03,960
kept in a folder that
you create an opt.

205
00:16:03,960 --> 00:16:07,240
So that jumped out to me right away and I
was like, well, that's really strange.

206
00:16:07,240 --> 00:16:10,640
Why now are they telling me to
keep it in a home directory?

207
00:16:10,640 --> 00:16:15,440
Whereas previously, it
was in an opt directory.

208
00:16:15,440 --> 00:16:20,380
So I don't, I don't know what that is, that
might be something to bring up in their issue

209
00:16:20,380 --> 00:16:27,400
tracker of, is that really necessary because
well, when I guess I think about all their

210
00:16:27,400 --> 00:16:32,440
YAML examples, they always say like path to
app directory, meaning you can put your path

211
00:16:32,440 --> 00:16:34,120
wherever you want.

212
00:16:34,120 --> 00:16:39,200
But for somebody who's just kind of getting
their feet wet, I see that as a disconnect

213
00:16:39,200 --> 00:16:44,840
from the documentation that I'm reading,
compared to what they're trying to preach.

214
00:16:44,840 --> 00:16:48,640
So that might be something where the
documentation could be improved.

215
00:16:48,640 --> 00:16:55,680
Then they go through the PUID and
PGID section, which was great.

216
00:16:55,680 --> 00:16:59,440
They always say like, you know, your value is
probably a thousand, but double check and

217
00:16:59,440 --> 00:17:01,320
make sure it's not
something different.

218
00:17:01,320 --> 00:17:07,280
So I'm glad I did check because that default user
of Ubuntu had a thousand and Rasta Calavera

219
00:17:07,280 --> 00:17:09,480
had a thousand and one.

220
00:17:09,480 --> 00:17:13,800
So in all of my compose files, if I'm running
them as Rasta Calavera rather than the default

221
00:17:13,800 --> 00:17:19,320
Ubuntu, then I need to use a thousand one if
I don't want to experience complications.

222
00:17:19,320 --> 00:17:22,840
So kudos to that
documentation, it was great.

223
00:17:22,840 --> 00:17:27,240
I know what I need to do.

224
00:17:27,240 --> 00:17:31,320
And after that, here
was that disconnect.

225
00:17:31,320 --> 00:17:37,080
So after when they talk about the PUID and
PGID, then they talk about volumes in more

226
00:17:37,080 --> 00:17:41,800
detail and their documentation again went
back to that same folder structure of opt

227
00:17:41,800 --> 00:17:44,160
app data container name.

228
00:17:44,160 --> 00:17:46,160
So I was like, okay, so
there is a disconnect.

229
00:17:46,160 --> 00:17:51,040
I don't know why they were doing the
home path instead of the opt path.

230
00:17:51,040 --> 00:17:56,800
So I think that's something I'm going to dig
into and ask a couple more questions on.

231
00:17:56,800 --> 00:18:01,480
But so that's getting through their
very basic introductory material.

232
00:18:01,480 --> 00:18:03,920
Now it's time to
get some swagger.

233
00:18:03,920 --> 00:18:10,360
So the key attractiveness of their swagger
setup is that it comes with a reverse proxy

234
00:18:10,360 --> 00:18:17,120
to kind of give you a layer of security and
it comes with automated SSL certificates.

235
00:18:17,120 --> 00:18:25,440
Now in the past, when I've tried things like
home lab OS or my own setup, just using a

236
00:18:25,440 --> 00:18:31,560
base Ubuntu image, using home lab,
they used a reverse proxy of traffic.

237
00:18:31,560 --> 00:18:36,240
And when I've tried to set up traffic
on my own, that was a nightmare.

238
00:18:36,240 --> 00:18:40,600
And their documentation
looks nice.

239
00:18:40,600 --> 00:18:44,520
And to an experienced person
probably reads really well.

240
00:18:44,520 --> 00:18:50,120
But for a new like myself, it's just way
too much and I don't understand it.

241
00:18:50,120 --> 00:18:56,600
So when I was doing my own setup, not using
home lab OS, I tried engine X proxy manager,

242
00:18:56,600 --> 00:19:02,920
which is a GUI front end to using a reverse
proxy and it handles SSL and everything else.

243
00:19:02,920 --> 00:19:04,520
And that worked
out really good.

244
00:19:04,520 --> 00:19:08,480
And that's what I ran for a long time
until my whole setup got fried.

245
00:19:08,480 --> 00:19:14,160
So I could have gone that route, but I decided
this is all going to be LSIO documentation.

246
00:19:14,160 --> 00:19:18,240
So I'm going to follow their
recommendation on how to do it.

247
00:19:18,240 --> 00:19:21,920
So getting it set
up was painless.

248
00:19:21,920 --> 00:19:23,320
It was so nice.

249
00:19:23,320 --> 00:19:27,560
I just had to modify the
Docker compose file.

250
00:19:27,560 --> 00:19:30,960
And in my written documentation, I put in
these little asterisks to kind of show you

251
00:19:30,960 --> 00:19:34,360
exactly what I changed.

252
00:19:34,360 --> 00:19:36,480
You have to have a domain.

253
00:19:36,480 --> 00:19:41,080
And there are ways to get around that
using like duck DNS and things like that.

254
00:19:41,080 --> 00:19:44,000
I have a domain through no IP.

255
00:19:44,000 --> 00:19:49,480
So in the URL section, I
put in my no IP domain.

256
00:19:49,480 --> 00:19:51,880
And then I just left the
other stuff in there.

257
00:19:51,880 --> 00:20:00,440
And then for the volumes, I use the app data
swag path rather than the home path Rasta

258
00:20:00,440 --> 00:20:03,080
Calavera app data swag.

259
00:20:03,080 --> 00:20:12,440
So that I'm going by that idea rather than the
home one, which I talked about previously.

260
00:20:12,440 --> 00:20:17,760
So after running the command, bring
the container up and follow the logs.

261
00:20:17,760 --> 00:20:18,760
Everything was working.

262
00:20:18,760 --> 00:20:25,880
I was able to visit my URL and see the engine
X welcome page perfect, painless, great.

263
00:20:25,880 --> 00:20:30,560
So now that the reverse proxy is running,
let's get a different container up and going.

264
00:20:30,560 --> 00:20:36,640
And I figured using code server or VS code
would be a good one to do because I've been

265
00:20:36,640 --> 00:20:39,920
using that a lot for all
of my written stuff.

266
00:20:39,920 --> 00:20:46,800
So I go to the code server documentation
and figure, all right, let's follow this.

267
00:20:46,800 --> 00:20:52,840
So I changed the variables in the Docker compose
to fit my situation using their example YAML.

268
00:20:52,840 --> 00:21:01,880
I add the custom sub domain for VS code
and the variables for VS code itself.

269
00:21:01,880 --> 00:21:09,320
And then to do the reverse proxy, you have
to go into the swag config engine X proxy

270
00:21:09,320 --> 00:21:13,920
configs and find the
service that you want.

271
00:21:13,920 --> 00:21:20,320
So for every image that LSIO provides, there's
also example configurations for the reverse

272
00:21:20,320 --> 00:21:22,160
proxy.

273
00:21:22,160 --> 00:21:30,760
So going into there, you just have to make sure
that you change the name of that to remove

274
00:21:30,760 --> 00:21:35,720
the example part, and then you put
in the necessary information.

275
00:21:35,720 --> 00:21:43,080
And then also in the subdomains of your original
YAML for the swag container, you have to put

276
00:21:43,080 --> 00:21:45,080
in what that subdomain
is going to be.

277
00:21:45,080 --> 00:21:58,640
So for example, if you wanted to be like vs.yourdomainname.com
in the swag YAML part under the subdomains

278
00:21:58,640 --> 00:22:03,920
where they just have www, you also got to put
in a comma there and then whatever you want

279
00:22:03,920 --> 00:22:07,400
the front end of
your service to be.

280
00:22:07,400 --> 00:22:11,800
So doing those things,
and it worked out great.

281
00:22:11,800 --> 00:22:17,560
And I did have to go back and look at the
documentation for swag, just to kind of reaffirm

282
00:22:17,560 --> 00:22:20,080
and make sure that I was
making the correct changes.

283
00:22:20,080 --> 00:22:23,800
But the way that it was organized, it was an
easy thing to reference and way faster than

284
00:22:23,800 --> 00:22:27,400
searching Google
or other stuff.

285
00:22:27,400 --> 00:22:33,360
One weird thing did happen when I was doing
VS code originally, I don't know if I made

286
00:22:33,360 --> 00:22:38,720
a silly mistake in my configs or something,
but I got locked out, couldn't execute any

287
00:22:38,720 --> 00:22:42,120
commands, had to do
a hard shutdown.

288
00:22:42,120 --> 00:22:46,840
And then when I rebooted, I just
figured, well, I screwed something up.

289
00:22:46,840 --> 00:22:48,760
So I'll just redo the YAML.

290
00:22:48,760 --> 00:22:51,560
And I did, and everything
worked just fine.

291
00:22:51,560 --> 00:22:53,040
So just one weird hiccup.

292
00:22:53,040 --> 00:22:58,400
And I don't really know what caused it or
why it occurred or whatever, but it was an

293
00:22:58,400 --> 00:23:00,240
easy thing to fix.

294
00:23:00,240 --> 00:23:07,040
So aside from getting Ubuntu to run on
a Pi, this process was fairly smooth.

295
00:23:07,040 --> 00:23:14,640
Even for a newbie, if you're somebody who's
new to this and you're okay with reading and

296
00:23:14,640 --> 00:23:20,800
attempting to comprehend documentation, I
think the LSIO documentation is some of the

297
00:23:20,800 --> 00:23:21,800
best out there.

298
00:23:21,800 --> 00:23:23,800
I would give it a
solid 10 out of 10.

299
00:23:23,800 --> 00:23:30,680
Its readability levels are very
approachable for somebody who's new.

300
00:23:30,680 --> 00:23:37,240
So if you want to get started, I recommend
you go this route with Linux server IO.

301
00:23:37,240 --> 00:23:43,480
So in general, some things that I think that
could be improved with their documentation

302
00:23:43,480 --> 00:23:50,240
is adding that little disclaimer about getting
Docker and Docker composed installed and then

303
00:23:50,240 --> 00:24:01,360
clearing up what the issue is with the slash
home slash user slash app data comparative

304
00:24:01,360 --> 00:24:05,960
to slash opt slash app data
slash container name.

305
00:24:05,960 --> 00:24:10,160
I would just like to see some clarification
for that and maybe some reasons why you would

306
00:24:10,160 --> 00:24:13,600
want it in one place
versus another.

307
00:24:13,600 --> 00:24:23,640
And also with the bash aliases file, apart
to moving that sooner, I believe, I haven't

308
00:24:23,640 --> 00:24:29,040
tested this yet, but I believe it to be true,
that the way that they have the bash aliases

309
00:24:29,040 --> 00:24:34,600
written out, it's referencing the
location of your Docker compose YAML.

310
00:24:34,600 --> 00:24:41,320
So if you are putting everything in opt app
data, I believe your YAML has to be there as

311
00:24:41,320 --> 00:24:48,960
well for the aliases to work, whereas if you
put everything in a home folder structure,

312
00:24:48,960 --> 00:24:53,440
then your bash aliases
file should reflect that.

313
00:24:53,440 --> 00:24:58,320
And the current way, if you just copy and
paste it from their website, it's using the

314
00:24:58,320 --> 00:25:01,120
opt file path.

315
00:25:01,120 --> 00:25:05,600
So I believe that to be a disconnect
to that needs to be cleared up.

316
00:25:05,600 --> 00:25:11,600
So I think my next steps are to just be installing
more containers, following documentation, making

317
00:25:11,600 --> 00:25:19,800
sure it all flows smoothly, and then maybe
put in some pull requests for those things

318
00:25:19,800 --> 00:25:21,480
that I mentioned.

319
00:25:21,480 --> 00:25:26,880
And if you try to replicate this, let me know
how you get along and see if there's anything

320
00:25:26,880 --> 00:25:29,720
else that we need to bring up.

321
00:25:29,720 --> 00:25:36,720
So that has been the end of episode
two for the Linux Lemming.

