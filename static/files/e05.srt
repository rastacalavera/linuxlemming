1
00:00:00,000 --> 00:00:17,840
Hello, and welcome to
the Linux Lemming.

2
00:00:17,840 --> 00:00:23,240
I'm your host, Rasta Calavera, and
here we are now at Episode 5.

3
00:00:23,240 --> 00:00:27,560
I know it's been a while since a release has
come out, so we're going to start today

4
00:00:27,560 --> 00:00:31,120
by talking about where
are my episodes.

5
00:00:31,120 --> 00:00:35,800
So my release schedule was recently
disrupted due to illness.

6
00:00:35,800 --> 00:00:40,720
Luckily, I've been healthy, but members of my
family and the academic institutions that

7
00:00:40,720 --> 00:00:45,760
they attend were impacted
by the COVID pandemic.

8
00:00:45,760 --> 00:00:52,600
Thankfully, nobody in my family contracted COVID,
but we had to practice quarantine procedure

9
00:00:52,600 --> 00:00:55,800
to keep ourselves and
our communities safe.

10
00:00:55,800 --> 00:01:00,680
So this took time away from the project, and
something like this may occur again in the

11
00:01:00,680 --> 00:01:01,680
future.

12
00:01:01,680 --> 00:01:07,680
So it's nice that I don't have a committed
release schedule, but again, as time allows,

13
00:01:07,680 --> 00:01:10,440
these episodes will continue.

14
00:01:10,440 --> 00:01:16,200
So moving on to our next topic, what
is the status with documentation?

15
00:01:16,200 --> 00:01:22,760
So very recently, I have been banging my head
against the wall, trying to get Wallabag

16
00:01:22,760 --> 00:01:25,640
to run on a Raspberry Pi 4.

17
00:01:25,640 --> 00:01:34,480
If you're not familiar with the Wallabag project,
it is an internet archiving bookmarking type

18
00:01:34,480 --> 00:01:43,560
piece of software, similar to other read-it-later
softwares like Pocket or Instapaper, Feedly

19
00:01:43,560 --> 00:01:50,440
to some extent, very similar
to those types of softwares.

20
00:01:50,440 --> 00:01:59,840
Some of the people in the Wallabag community
are very nurturing, and I'm very thankful

21
00:01:59,840 --> 00:02:01,640
to have them around.

22
00:02:01,640 --> 00:02:11,200
One big issue that I came up with was that there
is no ARM image for the Wallabag project.

23
00:02:11,200 --> 00:02:15,360
They don't offer anything for
the ARM platform by default.

24
00:02:15,360 --> 00:02:24,480
So you have to look for community Docker images
or even building your own Docker image.

25
00:02:24,480 --> 00:02:32,920
I went down the route of trying to find something
on Docker Hub, and I found a workable image.

26
00:02:32,920 --> 00:02:40,120
One key thing to note is that the latest tag in
Docker Hub is not actually the latest version

27
00:02:40,120 --> 00:02:41,840
of the project.

28
00:02:41,840 --> 00:02:45,640
So this, I've always heard about this being an
issue in the past and how you should always

29
00:02:45,640 --> 00:02:52,560
use the version tag that you want to run, but
I've never had a problem using the latest,

30
00:02:52,560 --> 00:02:58,880
and I think that is mainly because I stick
to Linux server images whenever possible.

31
00:02:58,880 --> 00:03:06,240
But in this image, I had to use a 2.4.2 tag
if I wanted the latest and greatest version

32
00:03:06,240 --> 00:03:12,680
of Wallabag, which I certainly did, because
once I did get the project up and running,

33
00:03:12,680 --> 00:03:19,920
there was a feature that I desperately wanted
where when I imported an OPML file, which

34
00:03:19,920 --> 00:03:28,360
is like all of my RSS feeds, they all came in
as unread, and I have read those and want

35
00:03:28,360 --> 00:03:37,520
to keep those, and there was no mass select
option in the older version of Wallabag.

36
00:03:37,520 --> 00:03:43,600
Actually that experimental feature just
came out about a month ago or so.

37
00:03:43,600 --> 00:03:48,360
So I desperately wanted
that new image.

38
00:03:48,360 --> 00:03:53,280
When I went out into the community, when I was
having problems getting it up and running,

39
00:03:53,280 --> 00:04:00,160
my biggest frustration is documented on
Reddit in the self-hosted subreddit.

40
00:04:00,160 --> 00:04:07,840
If anybody out there wants to see that whole
crusade for aid, a user did provide me a working

41
00:04:07,840 --> 00:04:13,180
compose file, so I had submitted what I was
currently using, and it had undergone some

42
00:04:13,180 --> 00:04:17,920
review and some tweaks, and that
really helped me troubleshoot.

43
00:04:17,920 --> 00:04:22,360
And I'm just very appreciative, and it's
always nice when someone takes that time and

44
00:04:22,360 --> 00:04:29,080
that extra care on an asynchronous platform
like Reddit or in a forum, and it brings

45
00:04:29,080 --> 00:04:34,920
me to the reflection of really
what I was doing wrong.

46
00:04:34,920 --> 00:04:43,320
So this is an instance where the problem
existed between the keyboard and the chair.

47
00:04:43,320 --> 00:04:49,780
Part of it was my own ignorance in not reading
very closely the documentation and having

48
00:04:49,780 --> 00:04:54,200
more confidence in my own abilities
than I actually possessed.

49
00:04:54,200 --> 00:04:59,440
So that brings us to what
were some of those problems.

50
00:04:59,440 --> 00:05:05,760
Wallabag does have documentation, but I didn't
like the approach of having to clone their

51
00:05:05,760 --> 00:05:13,160
repo and use their own compose file, and since
I was doing the LSIO swag reverse proxy,

52
00:05:13,160 --> 00:05:19,720
I like to follow their approach and stay as
close to that project as I possibly can.

53
00:05:19,720 --> 00:05:27,400
LSIO does provide a proxy config for Wallabag,
so that was a great place to start.

54
00:05:27,400 --> 00:05:34,600
One of my hangups is I don't like having the
full name of the service in front of my domain.

55
00:05:34,600 --> 00:05:40,920
So for example, in the LSIO proxy config and
in all of their configs, they always start

56
00:05:40,920 --> 00:05:44,440
the whole URL slug with
the name of the service.

57
00:05:44,440 --> 00:05:50,920
So in this instance, they're saying you
should have wallabag.mydomain.com.

58
00:05:50,920 --> 00:05:53,720
And I just, I think that
complicates things a bit.

59
00:05:53,720 --> 00:06:01,200
I think it opens up visibility on the wider
web, which I don't necessarily want.

60
00:06:01,200 --> 00:06:07,520
But once I got Wallabag working on the local
host, I wanted to be able to access it using

61
00:06:07,520 --> 00:06:09,760
my domain.

62
00:06:09,760 --> 00:06:16,920
And there are a few things that are very picky
about this process, both with the LSIO way

63
00:06:16,920 --> 00:06:22,280
of doing things and also with the
Wallabag way of doing things.

64
00:06:22,280 --> 00:06:30,440
So for Wallabag, there is an environment
variable that is the base URL or the domain.

65
00:06:30,440 --> 00:06:35,120
And that is extremely,
extremely picky.

66
00:06:35,120 --> 00:06:41,400
One thing that I encountered that was difficult
during the local host setup was you have to

67
00:06:41,400 --> 00:06:45,920
have the port number at
the end of your URL.

68
00:06:45,920 --> 00:06:56,200
So if your IP scheme is something like 10.10.0.1
for your router, and your actual computer

69
00:06:56,200 --> 00:07:05,840
is 10.10.0.4, then you would have to use that
IP plus the port of Wallabag, which is port

70
00:07:05,840 --> 00:07:09,640
80, to be able to
reach that locally.

71
00:07:09,640 --> 00:07:17,280
You could not just put in 10.10.0.4 and
reach your instance, it won't work.

72
00:07:17,280 --> 00:07:23,040
And then when you're running things in a container,
you typically have the exposed port and the

73
00:07:23,040 --> 00:07:24,780
internal port.

74
00:07:24,780 --> 00:07:31,080
So in your compose file, in your port section,
there are two numbers, one before the colon

75
00:07:31,080 --> 00:07:32,920
and one after the colon.

76
00:07:32,920 --> 00:07:38,560
So if you have nothing else running on port
80 externally, then your port scheme would

77
00:07:38,560 --> 00:07:40,560
be 8080.

78
00:07:40,560 --> 00:07:47,740
But for me, I have a lot of different
services, a lot that use 80 and 443.

79
00:07:47,740 --> 00:07:54,160
So you have to be very conscious
about how you set that up.

80
00:07:54,160 --> 00:07:59,840
So I spent a lot of time bringing containers
up and down and just messing with that domain

81
00:07:59,840 --> 00:08:01,400
name parameter.

82
00:08:01,400 --> 00:08:06,920
I switched it to HTTPS,
HTTP, my domain name.

83
00:08:06,920 --> 00:08:15,840
I tried my external Docker port of 80,000,
and I would always hit a 502 error from my

84
00:08:15,840 --> 00:08:18,040
swag instance.

85
00:08:18,040 --> 00:08:30,440
So what I did was just waste hours searching
online after messing with this domain thing.

86
00:08:30,440 --> 00:08:35,360
And then I figured, you know what, I'm just
going to leave that alone with my working

87
00:08:35,360 --> 00:08:40,840
host, and I just need to switch gears
and mess with the proxy config.

88
00:08:40,840 --> 00:08:43,040
And here's a really
important thing.

89
00:08:43,040 --> 00:08:47,160
If you're messing with your proxy configs and
your container names and everything else

90
00:08:47,160 --> 00:08:52,600
with the LSIO project, you need
to understand the naming scheme.

91
00:08:52,600 --> 00:08:56,000
And I've skimmed over
that in the past.

92
00:08:56,000 --> 00:08:59,320
And this is where it
finally came to bite me.

93
00:08:59,320 --> 00:09:06,040
If I had been a really careful reader,
it would have saved me lots of time.

94
00:09:06,040 --> 00:09:13,120
So if you look at the general template for
the subdomain for a proxy config from LSIO,

95
00:09:13,120 --> 00:09:20,880
and I have that copied and pasted in the blog
that accompanies this episode, they're very

96
00:09:20,880 --> 00:09:25,840
conscious about telling
you how to set it up.

97
00:09:25,840 --> 00:09:32,760
And if you are not paying close attention like
I did and figured you've done this enough

98
00:09:32,760 --> 00:09:40,520
times and it's not a big deal, really go back
and read that closely because they're very

99
00:09:40,520 --> 00:09:45,560
conscious about the container
names have to match.

100
00:09:45,560 --> 00:09:54,240
So in the actual template, they have that laid
out very clearly where it says container

101
00:09:54,240 --> 00:09:58,320
name, container name,
container name.

102
00:09:58,320 --> 00:10:05,800
And when I looked at my compose file,
I had my service defined as Wallabag.

103
00:10:05,800 --> 00:10:08,720
I had the image and then
I had the environment.

104
00:10:08,720 --> 00:10:14,200
I did not have a container name
variable in there at all.

105
00:10:14,200 --> 00:10:20,040
So that was a huge punch in the gut like,
oh my goodness, how did I screw that up?

106
00:10:20,040 --> 00:10:27,480
And if you look at another example compose
file for any of the LSIO images, you'll see

107
00:10:27,480 --> 00:10:30,240
the service defined name.

108
00:10:30,240 --> 00:10:33,280
And in my blog, I use the
example of audacity.

109
00:10:33,280 --> 00:10:39,320
So it'd be audacity, then the image and then
directly below the image is the container

110
00:10:39,320 --> 00:10:43,780
name and they put in audacity.

111
00:10:43,780 --> 00:10:52,120
So if you are changing it to anything else,
like say I wanted to call the Wallabag service

112
00:10:52,120 --> 00:11:01,360
keep, because I wanted to keep all of my articles,
then the container name has to be keep.

113
00:11:01,360 --> 00:11:08,720
I can't stay as the default Wallabag and you
have to make sure that those things match.

114
00:11:08,720 --> 00:11:15,560
So if I'm going to rename the service from
Wallabag to keep, my new compose scheme is

115
00:11:15,560 --> 00:11:22,680
going to start with a service called keep,
the image for Wallabag, and then a container

116
00:11:22,680 --> 00:11:25,320
name called keep.

117
00:11:25,320 --> 00:11:31,920
And all of that has to match the proxy config,
which means that you need to save your config

118
00:11:31,920 --> 00:11:36,880
file as keep.subdomain.conf.

119
00:11:36,880 --> 00:11:40,400
All of those things have to
be aligned with each other.

120
00:11:40,400 --> 00:11:47,240
And I'm sure experienced users are like,
well, duh, of course that all makes sense.

121
00:11:47,240 --> 00:11:52,160
And it makes sense to me now, but I'm sure
there are other people who will make the same

122
00:11:52,160 --> 00:12:00,360
mistake that I did as they try to
customize things to meet their needs.

123
00:12:00,360 --> 00:12:07,600
So once you get all of that aligned, you have
to go back to the Wallabag compose file.

124
00:12:07,600 --> 00:12:14,920
And in the symphony environment, you have to
change that domain name to an HTTPS, because

125
00:12:14,920 --> 00:12:21,520
it's going to be used by the swag service,
so it's going to be a secure connection.

126
00:12:21,520 --> 00:12:23,240
And you have to put
in your whole thing.

127
00:12:23,240 --> 00:12:27,560
So for me, it'd be like
keep.mydomain.com.

128
00:12:27,560 --> 00:12:34,800
And that was how I finally reached
the level of success that I needed.

129
00:12:34,800 --> 00:12:42,520
So in terms of documentation, like I mentioned
earlier, Wallabag does have documentation,

130
00:12:42,520 --> 00:12:50,920
but I think it could greatly be improved,
but I don't think I'm the one to do so.

131
00:12:50,920 --> 00:13:00,120
I feel like my approach is still cobbled together,
and especially because it's using a non-upstream

132
00:13:00,120 --> 00:13:02,160
image provided by the project.

133
00:13:02,160 --> 00:13:06,920
I don't feel like it would be appropriate for
me to provide documentation using another

134
00:13:06,920 --> 00:13:08,480
community member's image.

135
00:13:08,480 --> 00:13:11,960
I don't feel like
that's my place.

136
00:13:11,960 --> 00:13:21,400
And Wallabag change and provide their own
multi-arch service getting into the ARM platform,

137
00:13:21,400 --> 00:13:27,720
then maybe I would consider providing
documentation for how to get that up and running.

138
00:13:27,720 --> 00:13:32,640
But again, they kind of have their own way of
even documenting how they recommend reverse

139
00:13:32,640 --> 00:13:37,600
proxies are used using IngenX
and things like that.

140
00:13:37,600 --> 00:13:44,440
So because my use case is different using a
different project's reverse proxy solution,

141
00:13:44,440 --> 00:13:49,800
again, I don't know that it would
be my place to provide that.

142
00:13:49,800 --> 00:13:56,280
But who knows, things may change, and if they
do, then I'll be there with some documentation

143
00:13:56,280 --> 00:13:59,160
ready and waiting.

144
00:13:59,160 --> 00:14:03,560
I do think, though, rather than contributing to
the upstream project, it would be appropriate

145
00:14:03,560 --> 00:14:12,680
for me to provide this information on something
like Reddit or something like Discourse, where

146
00:14:12,680 --> 00:14:18,200
it's all community-based, people all have
their own use cases, and somebody may benefit

147
00:14:18,200 --> 00:14:20,840
from this struggle.

148
00:14:20,840 --> 00:14:29,480
So I will kind of push this out to those spaces
rather than directly to the upstream project.

149
00:14:29,480 --> 00:14:38,000
So as a little bonus for this episode, I have
been using Feedly as an RSS catcher for ten

150
00:14:38,000 --> 00:14:39,000
or more years.

151
00:14:39,000 --> 00:14:40,000
It's been forever.

152
00:14:40,000 --> 00:14:48,040
I started using it early in college and
just never switched away to anything else.

153
00:14:48,040 --> 00:14:53,520
And they have this great feature called Boards,
where you can save articles to a very specific

154
00:14:53,520 --> 00:14:54,520
board.

155
00:14:54,520 --> 00:14:58,200
It's essentially like tagging
something, and then it's easy to find.

156
00:14:58,200 --> 00:14:59,400
You can read it later.

157
00:14:59,400 --> 00:15:01,440
It never goes away.

158
00:15:01,440 --> 00:15:09,560
But when you export your RSS feeds from Feedly,
it doesn't keep any of the tags, and it doesn't

159
00:15:09,560 --> 00:15:16,200
keep them organized by board,
which is a huge bummer.

160
00:15:16,200 --> 00:15:21,800
And Wallabag, well, they do have an import
data section for a lot of different services.

161
00:15:21,800 --> 00:15:25,040
They don't have anything
directly for Feedly.

162
00:15:25,040 --> 00:15:31,760
And I will say, Feedly does not
make it easy to export your file.

163
00:15:31,760 --> 00:15:36,760
If you go and dig into the settings of Feedly,
you would expect to find an export button

164
00:15:36,760 --> 00:15:39,200
front and center,
and it's not there.

165
00:15:39,200 --> 00:15:44,680
It's kind of hidden off to the side above
your feeds directly, and you have to like

166
00:15:44,680 --> 00:15:48,920
click this little, I don't remember if it's
like an arrow or the three-button hamburger

167
00:15:48,920 --> 00:15:50,640
type thing.

168
00:15:50,640 --> 00:15:55,080
But it definitely could
be easier to find.

169
00:15:55,080 --> 00:16:05,120
So once I got that out of Feedly, it didn't do
what I wanted it to, because it just brought

170
00:16:05,120 --> 00:16:12,680
in all of my subscriptions, but none
of my organization from the board.

171
00:16:12,680 --> 00:16:14,680
And that's really
what I cared about.

172
00:16:14,680 --> 00:16:18,040
I can resubscribe to
RSS feeds on my own.

173
00:16:18,040 --> 00:16:25,960
That's not a big deal, but I want my saved data
that I have 10-plus years of time invested

174
00:16:25,960 --> 00:16:27,160
in.

175
00:16:27,160 --> 00:16:35,720
So I was really upset by this, and one thing
that I noticed in Feedly is when you're looking

176
00:16:35,720 --> 00:16:40,800
at a board, all of your saved articles are
there, and you can scroll down and scroll

177
00:16:40,800 --> 00:16:45,680
down and scroll down until your entire
history is present on the webpage.

178
00:16:45,680 --> 00:16:52,000
And I was thinking, well, there's got to be
a way to scrape this webpage and get things

179
00:16:52,000 --> 00:16:56,560
like the title of the article and
the URL that's associated with it.

180
00:16:56,560 --> 00:16:58,680
There has to be a
way to do that.

181
00:16:58,680 --> 00:17:06,160
So I did some digging online, and I found an
article from a data science provider about

182
00:17:06,160 --> 00:17:12,280
this exact thing, how they scrape websites to
grab the URLs and the titles of these things,

183
00:17:12,280 --> 00:17:17,160
and then they put them in a nice, neat table
for you so you can easily copy and paste them

184
00:17:17,160 --> 00:17:19,200
into a spreadsheet.

185
00:17:19,200 --> 00:17:26,720
So now that I can put things in a spreadsheet, I
can make a CSV file, which is pretty universal.

186
00:17:26,720 --> 00:17:34,200
And in this new RSS catcher and
in Wallabag, I can put that in.

187
00:17:34,200 --> 00:17:41,320
Now Wallabag, the only CSV import that they
support is through a service called Instapaper.

188
00:17:41,320 --> 00:17:50,800
So I had to create an Instapaper account and
then export my empty data from Instapaper just

189
00:17:50,800 --> 00:17:54,280
so I could see how they
format their CSV.

190
00:17:54,280 --> 00:18:00,040
And once I understood how they formatted it,
I could use the code that I found to scrape

191
00:18:00,040 --> 00:18:06,240
my feedly history and then put all of that
into an Instapaper format and then import

192
00:18:06,240 --> 00:18:08,480
it into Wallabag and boom.

193
00:18:08,480 --> 00:18:19,240
I had my 10 years of saved data now hosted on
my own land, which thank goodness for that.

194
00:18:19,240 --> 00:18:23,160
I enjoy the graphical
interface of feedly.

195
00:18:23,160 --> 00:18:25,720
I think they do a
lot of good things.

196
00:18:25,720 --> 00:18:31,000
But in terms of owning your
data, it's not really there.

197
00:18:31,000 --> 00:18:34,120
And that's kind of a big
disappointment for me.

198
00:18:34,120 --> 00:18:42,280
So I think now is the time for me to remove
myself from that ecosystem after all this

199
00:18:42,280 --> 00:18:47,760
time and really double down
on owning my own data.

200
00:18:47,760 --> 00:18:53,360
And I do like Wallabag because not only can
you tag the different articles, but you can

201
00:18:53,360 --> 00:19:00,160
put in annotations, you can export them in
different formats like PDF or EPUB, a whole

202
00:19:00,160 --> 00:19:02,040
bunch of different things.

203
00:19:02,040 --> 00:19:07,200
So the flexibility offered by
Wallabag is just phenomenal.

204
00:19:07,200 --> 00:19:11,360
And I will say that they
do have a hosted option.

205
00:19:11,360 --> 00:19:17,400
So if you don't want to go through the struggle
or the effort of hosting your own instance

206
00:19:17,400 --> 00:19:24,280
of Wallabag, they do have a paid option and
it's pretty reasonable considering it's an

207
00:19:24,280 --> 00:19:29,400
open source project and
everything that they offer.

208
00:19:29,400 --> 00:19:35,440
When you compare the price between Wallabag and
feedly, feedly is kind of tiered and Wallabag

209
00:19:35,440 --> 00:19:37,800
is a fixed price.

210
00:19:37,800 --> 00:19:44,600
And you can certainly get feedly for cheaper,
but you still don't have that ownership of

211
00:19:44,600 --> 00:19:48,040
your data even on
the paid tier.

212
00:19:48,040 --> 00:19:55,840
So I think paying more to own your
information is worth it in every respect.

213
00:19:55,840 --> 00:20:01,040
And I was getting very close to just saying
forget it and paying for that instance of

214
00:20:01,040 --> 00:20:11,000
Wallabag, but my perseverance and stubbornness
led me to finally getting my own self-hosted

215
00:20:11,000 --> 00:20:13,440
solution.

216
00:20:13,440 --> 00:20:26,240
So that brings us to
the end of episode 5.

