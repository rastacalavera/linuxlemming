1
00:00:00,000 --> 00:00:17,920
Hello, and welcome to
the Linux Lemming.

2
00:00:17,920 --> 00:00:21,440
I'm your host, Rasta Calavera.

3
00:00:21,440 --> 00:00:26,360
In episode 3 today, a couple
of things I want to cover.

4
00:00:26,360 --> 00:00:33,800
The first one that I'm really happy about is
the actual accepted documentation that this

5
00:00:33,800 --> 00:00:37,000
project has
accomplished so far.

6
00:00:37,000 --> 00:00:42,760
And then I want to dive into a little bit of
web analytics software, which is kind of

7
00:00:42,760 --> 00:00:46,040
what I'm experimenting
with right now.

8
00:00:46,040 --> 00:00:52,920
So right now I've got two and counting projects
that documentation has actually been accepted

9
00:00:52,920 --> 00:00:53,920
for.

10
00:00:53,920 --> 00:00:57,480
The first one is a
huge win for me.

11
00:00:57,480 --> 00:01:02,840
It's from the Linux server IO project, and
getting a merge into their project holds a

12
00:01:02,840 --> 00:01:05,400
very special place in my heart.

13
00:01:05,400 --> 00:01:10,080
They have a really great community and a team
of devs who are constantly fielding questions

14
00:01:10,080 --> 00:01:11,800
through their Discord.

15
00:01:11,800 --> 00:01:15,760
In my opinion, their documentation
is some of the best out there.

16
00:01:15,760 --> 00:01:21,200
So being considered a contributor
to that makes me very, very happy.

17
00:01:21,200 --> 00:01:25,920
And what I contributed, I talked about it in
a blog post previously, if you had a chance

18
00:01:25,920 --> 00:01:28,760
to read that.

19
00:01:28,760 --> 00:01:38,000
They had some bash aliases that made
administering their containers a lot easier.

20
00:01:38,000 --> 00:01:46,000
Things like typing DC up and DC down instead
of Docker compose, TACD, or Docker compose

21
00:01:46,000 --> 00:01:49,240
up TACD and Docker
compose down.

22
00:01:49,240 --> 00:01:52,040
And just little
things like that.

23
00:01:52,040 --> 00:01:57,680
And with the bash aliases that they had in
their documentation, it was referencing a

24
00:01:57,680 --> 00:02:01,000
path into the opt directory.

25
00:02:01,000 --> 00:02:07,320
So they basically recommend that you keep
all of the static volumes and things in a

26
00:02:07,320 --> 00:02:14,960
file path of opt app data, whatever the container
name is and config and all of that stuff.

27
00:02:14,960 --> 00:02:20,560
And I guess they assumed that the
YAML file will live there as well.

28
00:02:20,560 --> 00:02:25,280
But in my case, I always keep the
YAML file in my home directory.

29
00:02:25,280 --> 00:02:31,000
And then I keep all the container
stuff in the opt app data directory.

30
00:02:31,000 --> 00:02:36,560
And in years past, I've always
copied these bash aliases blindly.

31
00:02:36,560 --> 00:02:38,400
And they just never worked.

32
00:02:38,400 --> 00:02:42,480
And I just kind of assumed, well, whatever,
I'll just ignore those for now and type out

33
00:02:42,480 --> 00:02:44,000
the whole thing.

34
00:02:44,000 --> 00:02:50,000
But what I learned is that it
was referencing the wrong path.

35
00:02:50,000 --> 00:02:52,360
And I don't know why that
never clicked with me before.

36
00:02:52,360 --> 00:02:56,480
Maybe I'm just more experienced with
it now that it stood out to me.

37
00:02:56,480 --> 00:03:01,640
But I thought, well, geez, somebody who's
just getting started, they probably don't

38
00:03:01,640 --> 00:03:05,440
have their YAML in
that same file path.

39
00:03:05,440 --> 00:03:09,320
So it would make sense to kind of put in a
line of clarity there saying, like, depending

40
00:03:09,320 --> 00:03:15,120
on where your YAML file is stored, your bash
alias will have to change accordingly for

41
00:03:15,120 --> 00:03:16,120
that.

42
00:03:16,120 --> 00:03:18,680
So I put in documentation.

43
00:03:18,680 --> 00:03:23,400
I hopped onto their Discord and kind of threw
it out there and said, like, hey, what do

44
00:03:23,400 --> 00:03:24,720
you think about this?

45
00:03:24,720 --> 00:03:28,840
Do you think it's like a good
addition to the documentation?

46
00:03:28,840 --> 00:03:32,160
And a couple of users chimed
in, provided some feedback.

47
00:03:32,160 --> 00:03:34,680
I went in, made some edits.

48
00:03:34,680 --> 00:03:37,480
And lo and behold, it got
merged into the project.

49
00:03:37,480 --> 00:03:44,960
So now when you reference their Docker documentation,
you'll see my little commit there about pay

50
00:03:44,960 --> 00:03:49,240
attention to where your YAML is stored
and how it relates to the bash aliases.

51
00:03:49,240 --> 00:03:54,600
And I also provided a little code snippet to
show, OK, maybe it's in your home folder.

52
00:03:54,600 --> 00:03:59,840
So no matter where you are in the system, you
could be in the path of opt app data looking

53
00:03:59,840 --> 00:04:03,840
at proxy configs or
something for swag.

54
00:04:03,840 --> 00:04:09,120
And if you type DC up, it's still going to
hit the YAML file in your home directory.

55
00:04:09,120 --> 00:04:12,960
So that was just a nice
little contribution there.

56
00:04:12,960 --> 00:04:16,760
The second one is for
podcast generator.

57
00:04:16,760 --> 00:04:21,320
And this is another project very dear to me,
because when I first got into podcasting,

58
00:04:21,320 --> 00:04:26,080
and I like to call it a hobby cast, because
it was just me and a buddy chewing on fat,

59
00:04:26,080 --> 00:04:31,920
you know, and we had a listener pool of our
other group of friends, which is like three

60
00:04:31,920 --> 00:04:34,600
other people.

61
00:04:34,600 --> 00:04:40,240
But I used podcast generator for that project
to publish the podcast and actually get it

62
00:04:40,240 --> 00:04:45,200
into the iTunes store so that friends and
family could easily find it and listen to it

63
00:04:45,200 --> 00:04:48,240
if they so choose.

64
00:04:48,240 --> 00:04:57,880
And I wanted to spin that project up again to
actually send the podcast feed rather than

65
00:04:57,880 --> 00:05:02,160
GitLab through a VPS
that I spun up.

66
00:05:02,160 --> 00:05:07,800
Well, geez, I started with Linode, had a bunch
of issues with that, then I did Digital

67
00:05:07,800 --> 00:05:10,760
Ocean, had some
issues with that.

68
00:05:10,760 --> 00:05:13,880
There's a big blog post on that if you
want to know what those issues are.

69
00:05:13,880 --> 00:05:21,240
But long story short, I was running
into a whole bunch of problems.

70
00:05:21,240 --> 00:05:23,760
And I spent hours
searching online.

71
00:05:23,760 --> 00:05:29,040
And if you just do some web searching through
Google or whatever, you're really not going

72
00:05:29,040 --> 00:05:33,480
to find a lot of people talking about the
project, which surprised me because they've

73
00:05:33,480 --> 00:05:36,480
been around since like 2012.

74
00:05:36,480 --> 00:05:41,360
But it's just not something that comes up
a lot in forums and things like that.

75
00:05:41,360 --> 00:05:44,480
So I dug into their
issue tracker.

76
00:05:44,480 --> 00:05:54,080
And I found a post that was, I think it was
from 2020, so pretty recent, and it was people

77
00:05:54,080 --> 00:05:59,640
complaining about, I shouldn't say complaining,
it was people inquiring about similar issues

78
00:05:59,640 --> 00:06:02,920
to what I was having, but
not quite identical.

79
00:06:02,920 --> 00:06:09,960
And there was a user named Jay Adams, who
typed up what they were doing step by step

80
00:06:09,960 --> 00:06:12,200
by step to get it to work.

81
00:06:12,200 --> 00:06:19,320
And this was buried in a hyperlink that
led to their fork of the project.

82
00:06:19,320 --> 00:06:24,360
And after I found that, I was like, well,
shoot, this should just be in the official

83
00:06:24,360 --> 00:06:27,360
documentation.

84
00:06:27,360 --> 00:06:32,280
And if you look at Podcast Generator's website
under their self-hosted section, it's five

85
00:06:32,280 --> 00:06:33,280
lines.

86
00:06:33,280 --> 00:06:41,320
It's basically download the project, unzip
it, put it in your web server directory,

87
00:06:41,320 --> 00:06:42,680
and then you're good to go.

88
00:06:42,680 --> 00:06:45,600
And that's all they say.

89
00:06:45,600 --> 00:06:51,360
And I mean, granted, if you are a backend
administrator with web servers and Apache

90
00:06:51,360 --> 00:06:54,760
and all that stuff, it's
probably that easy.

91
00:06:54,760 --> 00:06:56,760
That's all you really
need to know.

92
00:06:56,760 --> 00:07:03,000
But for somebody who is just trying to get their
feet wet and get a project off the ground,

93
00:07:03,000 --> 00:07:08,160
it is not enough to get started
for somebody completely new.

94
00:07:08,160 --> 00:07:14,680
So I used Jay Adams documentation and then
modified it to kind of fit my use case and

95
00:07:14,680 --> 00:07:22,120
tried to make it accessible for somebody
who is also following that path.

96
00:07:22,120 --> 00:07:25,600
But even if you're not, I think there's enough
details there where you can stumble through

97
00:07:25,600 --> 00:07:29,080
it and make it work
for your use case.

98
00:07:29,080 --> 00:07:30,480
And that was a really
good addition.

99
00:07:30,480 --> 00:07:33,480
I had a great experience.

100
00:07:33,480 --> 00:07:40,040
I forked the project, made the edits, submitted
a pull request, got feedback from one of the

101
00:07:40,040 --> 00:07:44,600
devs, changed a few things, and
then it was accepted and merged.

102
00:07:44,600 --> 00:07:47,000
But it's not on their web page.

103
00:07:47,000 --> 00:07:54,040
So the installation that I wrote up is in
their installation file, and it's just a text

104
00:07:54,040 --> 00:07:56,720
file that's included
with the project.

105
00:07:56,720 --> 00:08:02,800
It's not even in markdown, which I thought was
kind of strange because I formatted everything

106
00:08:02,800 --> 00:08:06,120
in markdown just assuming
it was a markdown file.

107
00:08:06,120 --> 00:08:14,640
And then when I looked at the actual merge, I
was like, oh, well, this is just a text file.

108
00:08:14,640 --> 00:08:16,920
So now all my writing
looks kind of strange.

109
00:08:16,920 --> 00:08:24,240
But anyway, after I got that all set up and
squared away, I was kind of revisiting things

110
00:08:24,240 --> 00:08:30,800
and I got the project up and running, but I
couldn't upload any files through their web

111
00:08:30,800 --> 00:08:32,640
admin interface.

112
00:08:32,640 --> 00:08:37,000
And I even struggled
uploading things via FTP.

113
00:08:37,000 --> 00:08:40,440
So I've got that all
figured out now.

114
00:08:40,440 --> 00:08:54,120
One of the big issues is in the php.ini configuration,
there is a limit to the file size that you

115
00:08:54,120 --> 00:08:56,680
can upload, and it is
ridiculously tiny.

116
00:08:56,680 --> 00:08:59,520
I think it's like two
megabytes standard.

117
00:08:59,520 --> 00:09:03,360
So I bumped that up
to like 20,000.

118
00:09:03,360 --> 00:09:06,360
And then after that,
things started working.

119
00:09:06,360 --> 00:09:09,800
So that needs to be included
in the documentation as well.

120
00:09:09,800 --> 00:09:16,160
And then there was also something too with
the user permissions, the command I put in,

121
00:09:16,160 --> 00:09:20,240
I thought would apply the permission
down throughout the entire directory.

122
00:09:20,240 --> 00:09:26,080
I'm still pretty confident it does, but I'm
kind of at the point now where I want to blow

123
00:09:26,080 --> 00:09:32,960
everything away, follow my own documentation
again and make sure that that's accurate before

124
00:09:32,960 --> 00:09:36,720
I do another pull
request on there.

125
00:09:36,720 --> 00:09:41,280
So that's going to be up
and coming with that.

126
00:09:41,280 --> 00:09:45,760
So those are the two things that have actually
been like accepted and merged into projects

127
00:09:45,760 --> 00:09:46,760
right now.

128
00:09:46,760 --> 00:09:52,760
I've also got some conversations
going with some other devs.

129
00:09:52,760 --> 00:09:59,480
I've been digging into web analytics software
just because I'm curious, are people actually

130
00:09:59,480 --> 00:10:05,080
listening to this or visiting this and I don't
want to use Google Analytics, I just, I get

131
00:10:05,080 --> 00:10:11,480
kind of, I know it's really easy to do, but
with all the privacy and things that people

132
00:10:11,480 --> 00:10:17,720
are concerned about now and my own kind of
moral guidelines, I just, I want to steer

133
00:10:17,720 --> 00:10:19,680
clear of that if possible.

134
00:10:19,680 --> 00:10:25,800
So I found out that there's this whole kind of
ecosystem with open web analytics and there's

135
00:10:25,800 --> 00:10:31,640
a couple big name projects out there, Matimo,
I believe it's pronounced, is a huge one that

136
00:10:31,640 --> 00:10:38,000
a lot of people use and that
one is free as in beer.

137
00:10:38,000 --> 00:10:43,080
To a certain extent, they have like a limited
free tier and then if you want more additional

138
00:10:43,080 --> 00:10:47,800
features, they have
a paid version.

139
00:10:47,800 --> 00:10:54,320
So I guess maybe freemium would be
like a better way to phrase that.

140
00:10:54,320 --> 00:11:00,560
There's another one called plausible, which again
is kind of the same model and then there's

141
00:11:00,560 --> 00:11:06,520
another one called shy net, which is
completely free as in freedom and beer.

142
00:11:06,520 --> 00:11:15,760
So that one is supposedly feature parody on
with like Matimo, but there's no freemium

143
00:11:15,760 --> 00:11:17,640
aspect to it.

144
00:11:17,640 --> 00:11:25,440
The only problem I had with shy net is that
it's only built for x86 architecture, so

145
00:11:25,440 --> 00:11:30,480
I'm trying to run everything off of my raspberry
pi and currently I couldn't run shy net off

146
00:11:30,480 --> 00:11:39,560
of that platform and I couldn't find anything
on Docker hub that had builds for arm 64.

147
00:11:39,560 --> 00:11:48,560
So I installed it on an x86 machine and then
I had to try to figure out how to get swag

148
00:11:48,560 --> 00:11:57,760
to send requests to that machine and
that is kind of a mixed bag of tricks.

149
00:11:57,760 --> 00:12:04,760
I eventually got swag to communicate with it
so that if I put in something like shy net

150
00:12:04,760 --> 00:12:16,960
dot my URL dot com, it takes me to the actual web
GUI, but I still wasn't getting any analytics.

151
00:12:16,960 --> 00:12:27,600
And the snippet is a piece of HTML that you're
supposed to put into, I believe it's the head

152
00:12:27,600 --> 00:12:34,440
area of the HTML of a page and the way that
I'm doing it with Hugo, everything's in

153
00:12:34,440 --> 00:12:41,040
mark down, so I was really confused as to
how to find the correct HTML tags, so I had

154
00:12:41,040 --> 00:12:45,880
to do a lot of inspecting elements and page
source and Firefox to try to find where those

155
00:12:45,880 --> 00:12:55,520
things live and then I had to figure out more
with Hugo's architecture with their defaults

156
00:12:55,520 --> 00:13:01,320
and I learned a lot with that, a lot of that
lives in my theme of cast in it and you have

157
00:13:01,320 --> 00:13:06,240
to go into like page layouts and there's like
a whole little header and footer section

158
00:13:06,240 --> 00:13:10,120
that's just straight up HTML and then you got
to find the tags in there and paste your

159
00:13:10,120 --> 00:13:15,840
snippet in there, but I did eventually figure
that all out but I still wasn't getting any

160
00:13:15,840 --> 00:13:21,160
analytical data, so I'm kind of at a stand
still with that right now and the dev is kind

161
00:13:21,160 --> 00:13:27,440
of waiting for my response back, so I'm really
positive that the dev is interested and wants

162
00:13:27,440 --> 00:13:32,920
to help and things like that, but I've kind
of hit my time limit investment with that

163
00:13:32,920 --> 00:13:35,800
currently, so hopefully I
can return to that later.

164
00:13:35,800 --> 00:13:44,400
I did get Matimo up and running because that one
has an arm build, but I hit a lot of roadblocks

165
00:13:44,400 --> 00:13:52,960
with that too, didn't play nicely with swag
and when I eventually did get it to work,

166
00:13:52,960 --> 00:14:00,680
it still wasn't collecting any analytical
data, so I'm just assuming that I'm still

167
00:14:00,680 --> 00:14:06,880
speaking into the void and hopefully in the
near future I can figure out web analytics

168
00:14:06,880 --> 00:14:14,480
software and put something out with that,
but that's where the project is right now.

169
00:14:14,480 --> 00:14:20,920
If anybody in the community is out there and
wants me to try to attempt something or you

170
00:14:20,920 --> 00:14:24,720
have attempted something and you want to talk
about it, anything like that, contributions

171
00:14:24,720 --> 00:14:31,120
are always welcome through the Get Lab page,
you can submit a pull request there and we'll

172
00:14:31,120 --> 00:14:32,920
get something up and running.

173
00:14:32,920 --> 00:14:38,200
To try to speed on that community involvement,
I was exploring a platform called Comento

174
00:14:38,200 --> 00:14:46,920
that allows you to embed a commenting system
in a blog and a whole bunch of roadblocks

175
00:14:46,920 --> 00:14:48,680
with that as well.

176
00:14:48,680 --> 00:14:56,240
So I got a huge backlog of things to try to
troubleshoot, but until I come up with solutions

177
00:14:56,240 --> 00:15:22,640
for that, this will take us to the end
of episode 3 of the Linux Lemming.

