+++
Description = "Blog Post used to guide my recording of [Episode 07](https://linuxlemming.com/e07/) of the podcast"
Date = 2022-11-11T14:40:33.000Z
PublishDate = 2022-11-11T14:40:33.000Z
title = "The Return of the Lemming"
blog_image = "img/episode/e07/e07.png"
images = [ "img/episode/default-social.jpg" ]
Author = "rastacalavera"
tags = [ "announcement", "Value4Value", "blogcast" ]
preview = "/img/episode/e07/e07.png"
draft = "true"
+++
The revival of the podcast brings a lot of new topics. I am mainly really excited about [Podcast 2.0](https://github.com/Podcastindex-org/podcast-namespace/blob/main/podcasting2.0.md) features and I have been experimenting on how to implement some of the features in the [castanet theme](https://github.com/mattstratton/castanet/issues/391) that I use on my static site.

Things I want to put into the podcast:
* Value tag
    * Want to self host my own bitcoin/lightening node using Umbrel.
    * Been waiting about 3 months for the node to sync . . . 
* Chapter tags
    * Need to find an easy way to do this so that I don't have to make them manually otherwise I just won't do it.
* Lit tag
    * I've got pretty good traction with this using Azuracast. Just have to figure out how to do the whole podping thing (not even sure if that is needed).
* Transcripts tag
    * Got good traction on this one!
    * Got it [working!](https://gitlab.com/rastacalavera/linuxlemming/-/commit/4ef9ab98e76622bac9af353420f254894140cbc9)
        * Issues with [blink based browsers](https://gitlab.com/rastacalavera/linuxlemming/-/issues/3) though . . . 


I figured that adding transcription to all my episodes would be pretty easy and a simple thing to do. I dug into [Whisper](https://github.com/openai/whisper) which is an openai project that has high accuracy transcription that runs locally on your own hardware. I ran the base model on my laptop at it took about 4x the time of the episode to render out. I also learned that it puts 38 characters as a limit and the namespace requires a 32 character limit. To get around this, I found [subtitle composer](https://subtitlecomposer.kde.org/) which will easily take the srt file and change the character count. I ran all my episodes through this so that it meets the criteria. I played with the theme and think I found a good way to get them into the episodes. I would need to use [Podverse](https://podverse.fm/podcast/k9uvCAPrQ3) to verify that they are working since [Fountain](https://fountain.fm/) doesn't support it yet. While I was trouble shooting getting transcripts to work, the podverse Devs actually help me trouble shoot issues on [Mastadon](https://podcastindex.social/@podverse) and identify unexpected issues too (see link above in list if you want to know more).


As I gain advances into the Podcast 2.0 namespace, I am trying to find ways to contribute back to the [Main Castanet theme](https://github.com/mattstratton/castanet/issues/391). I am not a Dev so I'm not sure if the best approach is to make changes to theme or to use over rides in my main project. I've been watching an [OLD playlist on Youtube](https://www.youtube.com/watch?v=qtIqKaDlqXo&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) that is helping me to better understand the ins and outs of Hugo.

### So what to expect over all going forward?

I am going to try to categorize things and have a focus on specific topics. These will kind of be like "seasons" I guess. I think I'll do blog posts first and then when those are all done, start recording the podcast versions. I think this is a good approach because I find that basing the podcast off of a blog post keeps me focused on content and helps to streamline the production process.


Speaking of my production process . . . I had such a good method previously and now I am starting from scratch. As I refine new techniques and approaches, I'll be sure to document things so that others can copy it if they want. I think a good starting point will be to use some of the settings that Joshua Dennis and Steve Mclaughlin discuss in Episode 24 of [Podcasting 4 Value](https://podcastindex.org/podcast/4890674) and modify as needed.


So in terms of documentation lately . . . 
* Azuracast is a cool tool that I'll walk through soon
* I've got another PR waiting at LSIO for [Project Send](https://github.com/linuxserver/docker-projectsend/pull/27). That was a *rough* process and I think I'll stay away from that project in terms of documentation for the foreseeable future.
* I've contributed to the [SSH Wiki](https://github.com/selfhostedshow/wiki/pull/129) and I'll talk about that whole process and what I hope to bring with future contributions.
* Jupiter Broadcasting opened up their website for their entire podcast network to the [community for contribution on Github](https://github.com/JupiterBroadcasting/jupiterbroadcasting.com)
    * Such a cool thing to do and hopefully I can get involved in that project down the road.

### What's my setup now?

I use to run everything on a Raspberry Pi 4 with 4GB of ram and an external HDD connected via a powered USB hub. I retired that setup in favor of a Lenovo ThinkCentre Tiny with an i5, 16GB DDR3 ram and 2TB HDD. I am still running the Pi but now it is a dedicated [Umbrel Sever](https://umbrel.com/) and I don't know how long that will last, I haven't been too impressed with the speed, but in fairness I have been using a 1TB HDD and do plan on switching it out for a SSD once my bitcoin node finishes syncing. 


I also purchased a Synology NAS (DS418) off of eBay for a great price and currently have 2 8TB disks and a 2TB disk installed. I am using it to backup computers, photos, media, etc. I hope to pick up another 8TB HDD (or two) this season to completely fill it up. 


I made these two rather large purchases because I literally didn't have any extra time for tinkering. Between changing careers and the busyness of family life, I wanted solutions that just worked and so far I've been super happy with each.


My docker container usage has shrunk drastically since I don't have much time for tinkering any more either. Right now I have four containers and when I ran the pi fully time I think I averaged 12, so this has been a drastic step back. I do want to slowly bring back some of the apps that I abandoned during the migration, like mealie,wallabag, linkace, youtube-material, etc. One thing that is holding me back is that I didn't keep my original compose or .env file so a lot of the volumes I backed up are useless because I used complex passwords for services that I can't remember. Lesson learned I guess, I am just slow to action.


Speaking of backups, since I got the NAS now, I have been exploring the best way to backup my files to it from my server. I think I have settled on Rsync and will do a write up on that at some point.

I am currently using [Zorin](https://zorin.com/os/) as my daily driver and that can do automatic backups using Duplicity and my NAS system. Why Zorin? I purchased a license for it during a DLN charity stream a few years ago and since then I buy a license with each release. I rarely used it but thought it was a good organization so I like to support them. I figured that since I have a paid copy, I should put some milage behind it.

Well, that seems to take us to the end of this episode. Until next time, stay safe and stay curious.