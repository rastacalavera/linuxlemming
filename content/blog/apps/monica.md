+++
Description = "A Personal Relationship Management tool to help you document your social life."
Date = 2022-11-17T15:12:13-06:00
PublishDate = 2022-11-17T15:12:13-06:00 # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
draft = true
title = "Monica CRM"
#blog_banner = ""
blog_image = "img/host/calaveraJP.jpeg"
images = ["img/episode/default-social.jpg"]
Author = "rastacalavera"
#aliases = ["/##"]
series = ["selfhosted Apps"]
tags = ["docker","selfhosted","selfhosting","CRM"]
+++
I've heard about Monica CRM for years but never really gave it a go. It sounds interesting to me to have a little storage of information about friends and family that isn't just a calendar with repeating birthday events or a random note on my phone.

[Official Documentation](https://github.com/monicahq/docker)

Can't access the instance from IP:port. Documentatioin says to use localhost:8080 which I can't do.