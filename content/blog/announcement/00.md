+++
Description = "After some time away, the lemming has returned. This is a post mortem, mea culpa, etc. to make good on what occurred and what to expect going forward."
#Date = 2022-11-07T21:13:35-06:00
PublishDate = 2022-11-09T08:51:00-06:00 # this is the datetime for the when the episode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
title = "The Podcation is Over, Time to Return to Work."
#blog_banner = ""
blog_image = "img/host/calaveraJP.jpeg"
images = ["img/episode/default-social.jpg"]
Author = "rastacalavera"
#aliases = ["/##"]
series = ["announcement"]
tags = ["Value4Value", "features", "updates"]
+++
# What Happened?

Covid derailed my life. It wasn't that it almost killed me or anything, it was just the popping off point to a whole lot of other things happening. 

Since the podcast took a hiatus, I have had two different jobs, kids started primary school and life just went full throttle. Well, things are settling down a bit now and I think that I can start to find a groove again.


Since the podcast paused, I have learned a lot about [Podcasting 2.0](https://github.com/Podcastindex-org/podcast-namespace/blob/main/podcasting2.0.md) and [the Value for Value model](https://value4value.info/) and since learning about these things, I got a renewed energy to return to this passion project. If I was going to bring this thing back, I was going to bring it back and lean into some of these approaches.


I also need to figure out the theme/hosting situation. I've been using Castanet as a hugo theme and publishing with GitLab pages, but [Castopod](https://castopod.org/) looks like a very promising platform . . . my biggest issue is time availability to explore and learn. I basically have very little and I learn pretty slow which isn't a good combination for what I am trying to accomplish. But then again, this is just for me so I guess I should be happy with whatever I produce.


*IF* I stick with Hugo, I would like to learn a bit more about it so that I can comfortably edit the theme and make modifications to make it play nice with Podcast 2.0 Namespaces, like the [value block](https://github.com/Podcastindex-org/podcast-namespace/blob/main/value/value.md).


I am not a bitcoin person but the value block seems like a really cool idea and it's not too difficult to get something put together using [Umbrel](https://umbrel.com/) so once I finish getting comfortable with that, then I think things will really start to take off.

## So What Will the Revival Bring?

Here's what I'm thinking:
* Series
    * I want to try to use the taxonomy of a `"series"` from the Hugo theme and also to help shape my project goals. There is so much cool stuff that I think having a dedicated theme or series will help me stay focused and help to keep progressing.
* Value for Value
    * This whole show was constructed with community input in mind and it will stay that way. The may way I envision contribution is via gitlab/github but I may explore other options.
    * I will have a self-hosted bitcoin node with the lightening network enabled so that boosts and streaming sats can be contributed.
        * This will give me the ability to add splits to people who contribute!!! 
* Podcast Features
    * LIT tag
    * Chapters
        * Chapter art
    * TxT
        * Feed ownership verification
* Bi-weekly release
    * I want a consistent release schedule so this is what I am aiming for in the long run. This could be either a blog post OR a podcast starting out. Once a rhythm is found, regularity will follow.
* Grow the audience and participation.
    * Do more community outreach, increase visibility to find others who are like minded or interested.
