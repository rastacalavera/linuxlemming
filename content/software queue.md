+++
date = "2023-01-03T02:11:48-05:00"
description = "Software to Try Out"
title = "Software Queue"
+++
## Sys Admin Stuff

### [Script Server](https://github.com/bugy/script-server)

 Script-server is a Web UI for scripts.
As an administrator, you add your existing scripts into Script server and other users would be able to execute them via a web interface. The UI is very straightforward and can be used by non-tech people.
No script modifications are needed - you configure each script in Script server and it creates the corresponding UI with parameters and takes care of validation, execution, etc.

[DEMO server](https://script-server.net/)

[Admin interface screenshots](https://github.com/bugy/script-server/wiki/Admin-interface)

### [Docker-Cronicle-Docker](https://github.com/bluet/docker-cronicle-docker)

Run Cron jobs in docker containers.
Workflow scheduler to run docker jobs just like cron, but inside own container, with Cronicle in docker. Run dockerized Cronicle cron jobs in docker container.

### [Uptime-Kuma](https://github.com/louislam/uptime-kuma)-

It is a self-hosted monitoring tool like "Uptime Robot". Try it!

    Tokyo Demo Server: https://demo.uptime.kuma.pet (Sponsored by Uptime Kuma Sponsors)
    Europe Demo Server: https://demo.uptime-kuma.karimi.dev:27000 (Provided by @mhkarimi1383)

It is a temporary live demo, all data will be deleted after 10 minutes. Use the one that is closer to you, but I suggest that you should install and try it out for the best demo experience.

----

### [Monica](https://github.com/monicahq/monica)

Monica is an open-source web application to organize and record your interactions with your loved ones. We call it a PRM, or Personal Relationship Management. Think of it as a CRM (a popular tool used by sales teams in the corporate world) for your friends or family.

[PENDING ISSUES](https://github.com/monicahq/monica/discussions/6507)


### [Kavita Reader](https://www.kavitareader.com/)

Lighting fast with a slick design, Kavita is a rocket fueled self-hosted digital library which supports a vast array of file formats. Install to start reading and share your server with your friends. | [Demo](https://demo.kavitareader.com/)- Username: demouser Password: Demouser64 |

### [YTDL-Sub](https://github.com/jmbannon/ytdl-sub)

Automate downloading and metadata generation with YoutubeDL. ytdl-sub is a command-line tool that downloads media via yt-dlp and prepares it for your favorite media player, including Kodi, Jellyfin, Plex, Emby, and modern music players. No additional plugins or external scrapers are needed.

We recognize that everyone stores their media differently. Our approach for file and metadata formatting is to provide maximum flexibility while maintaining simplicity. | [Docs](https://ytdl-sub.readthedocs.io/) 


### [Web-Whisper](https://codeberg.org/pluja/web-whisper)

 A light user interface for OpenAI's Whisper right into your browser! [Demo](https://whisper.r3d.red/)

### [Memos](https://github.com/usememos/memos)

An open-source, self-hosted memo hub with knowledge management and socialization. [Demo](https://demo.usememos.com/)


### [Homebox](https://github.com/hay-kot/homebox)

Homebox is the inventory and organization system built for the Home User. [Demo](https://homebox.fly.dev/) | [Docs](https://hay-kot.github.io/homebox/)


### [Change Detection.io](https://github.com/dgtlmoon/changedetection.io)

Live your data-life pro-actively, Detect website changes and perform meaningful actions, trigger notifications via Discord, Email, Slack, Telegram, API calls and many more.

The best and simplest self-hosted free open source website change detection, monitor and notification service. Restock Monitor, change detection. Designed for simplicity - the main goal is to simply monitor which websites had a text change for free. Free Open source web page change detection, Restock Monitoring, Visualping and Apify alternative 

